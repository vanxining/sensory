﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(LineSensor))]
public class LineSensorEditor : Editor
{
    SerializedProperty lineLength;
    SerializedProperty CanPenetrate;
    SerializedProperty IgnoreLayers;

    SerializedProperty OnColliderFound;
    SerializedProperty OnColliderEnter;
    SerializedProperty OnColliderStay;
    SerializedProperty OnColliderExit;

    SerializedProperty alwaysDrawGizmos;
    SerializedProperty gizmoColor;

    static LineSensor sensor;
    static Transform sensorTransform;

    void OnEnable()
    {
        lineLength = serializedObject.FindProperty("lineLength");
        CanPenetrate = serializedObject.FindProperty("CanPenetrate");
        IgnoreLayers = serializedObject.FindProperty("IgnoreLayers");

        OnColliderFound = serializedObject.FindProperty("OnColliderFound");
        OnColliderEnter = serializedObject.FindProperty("OnColliderEnter");
        OnColliderStay = serializedObject.FindProperty("OnColliderStay");
        OnColliderExit = serializedObject.FindProperty("OnColliderExit");

        alwaysDrawGizmos = serializedObject.FindProperty("alwaysDrawGizmos");
        gizmoColor = serializedObject.FindProperty("gizmoColor");

        sensor = (LineSensor)target;
        if (sensor != null)
        {
            sensorTransform = sensor.transform;
        }
    }

    private bool eventFoldout;
    private bool gizmoFoldout;
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.PropertyField(lineLength);
        EditorGUILayout.PropertyField(CanPenetrate);
        EditorGUILayout.PropertyField(IgnoreLayers);
        EditorGUILayout.EndVertical();

        eventFoldout = EditorGUILayout.Foldout(eventFoldout, "Events");
        if (eventFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(OnColliderFound);
            EditorGUILayout.PropertyField(OnColliderEnter);
            EditorGUILayout.PropertyField(OnColliderStay);
            EditorGUILayout.PropertyField(OnColliderExit);
            EditorGUILayout.EndVertical();
        }

        gizmoFoldout = EditorGUILayout.Foldout(gizmoFoldout, "Gizmos");
        if (gizmoFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(alwaysDrawGizmos);
            EditorGUILayout.PropertyField(gizmoColor);
            EditorGUILayout.EndVertical();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
