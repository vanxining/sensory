﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(AdvancedSensor))]
[DisallowMultipleComponent]
public class AdvancedSensorEditor : Editor
{
    SerializedProperty sensorProp;
    SerializedProperty refreshRate;
    SerializedProperty layers;
    SerializedProperty tagSearch;
    SerializedProperty updateMode;
    SerializedProperty SensorDetectionMode;
    SerializedProperty foundColliders;
    SerializedProperty foundRigidbodies;

    static AdvancedSensor sensor;
    static Transform sensorTransform;

    void OnEnable()
    {
        sensorProp = serializedObject.FindProperty("sensor");
        refreshRate = serializedObject.FindProperty("refreshRate");
        layers = serializedObject.FindProperty("checkForLayers");
        tagSearch = serializedObject.FindProperty("tagSearch");
        updateMode = serializedObject.FindProperty("updateMode");
        SensorDetectionMode = serializedObject.FindProperty("SensorDetectionMode");
        foundColliders = serializedObject.FindProperty("foundColliders");
        foundRigidbodies = serializedObject.FindProperty("foundRigidbodies");

        sensor = (AdvancedSensor)target;
        if (sensor != null)
        {
            sensorTransform = sensor.transform;
        }
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.PropertyField(sensorProp);

        //Clamp the refreshrate to positive values
        sensor.refreshRate = Mathf.Clamp(sensor.refreshRate, 0, float.PositiveInfinity);

        EditorGUILayout.PropertyField(refreshRate);
        EditorGUILayout.PropertyField(updateMode);

        EditorGUILayout.BeginVertical(GUI.skin.box);
        if (!EditorApplication.isPlaying) //Disable in play mode
        {
            //sensor.layers = EditorGUILayout.LayerField("Layers", sensor.layers);
            EditorGUILayout.PropertyField(layers);
        }

        if (!EditorApplication.isPlaying) //Disable in play mode
        {
            EditorGUILayout.PropertyField(SensorDetectionMode);
        }

        if(sensor.GetDetectionMode() == AdvancedSensor.DetectionMode.Tag 
            && !EditorApplication.isPlaying)
        {
            sensor.tagSearch = EditorGUILayout.TagField("Tag", sensor.tagSearch);
        }
        EditorGUILayout.EndVertical();


        if (sensor.GetDetectionMode() == AdvancedSensor.DetectionMode.Collider ||
            sensor.GetDetectionMode() == AdvancedSensor.DetectionMode.Tag)
        {
            GUI.enabled = false; //Make read only
            EditorGUILayout.PropertyField(foundColliders);
            GUI.enabled = true;
        }

        if (sensor.GetDetectionMode() == AdvancedSensor.DetectionMode.Rigidbody)
        {
            GUI.enabled = false; //Make read only
            EditorGUILayout.PropertyField(foundRigidbodies);
            GUI.enabled = true;
        }

        EditorGUILayout.EndVertical();


        serializedObject.ApplyModifiedProperties();
    }
}
