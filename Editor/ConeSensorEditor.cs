﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(ConeSensor))]
public class ConeSensorEditor : Editor
{
    SerializedProperty precision;
    SerializedProperty visibilityThreshold;
    SerializedProperty IgnoreLayers;
    SerializedProperty UseLineOfSight;

    SerializedProperty radius;
    SerializedProperty allowedAngle;
    SerializedProperty maximalDetectableObjects;

    SerializedProperty OnColliderFound;
    SerializedProperty OnColliderEnter;
    SerializedProperty OnColliderStay;
    SerializedProperty OnColliderExit;

    SerializedProperty alwaysDrawGizmos;
    SerializedProperty arcColor;
    SerializedProperty gizmoColor;

    static ConeSensor sensor;
    static Transform sensorTransform;

    void OnEnable()
    {
        precision = serializedObject.FindProperty("precision");
        visibilityThreshold = serializedObject.FindProperty("visibilityThreshold");
        UseLineOfSight = serializedObject.FindProperty("UseLineOfSight");

        radius = serializedObject.FindProperty("radius");
        allowedAngle = serializedObject.FindProperty("angle");
        maximalDetectableObjects = serializedObject.FindProperty("maximalDetectableObjects");

        OnColliderFound = serializedObject.FindProperty("OnColliderFound");
        OnColliderEnter = serializedObject.FindProperty("OnColliderEnter");
        OnColliderStay = serializedObject.FindProperty("OnColliderStay");
        OnColliderExit = serializedObject.FindProperty("OnColliderExit");

        alwaysDrawGizmos = serializedObject.FindProperty("alwaysDrawGizmos");
        arcColor = serializedObject.FindProperty("arcColor");
        gizmoColor = serializedObject.FindProperty("gizmoColor");

        IgnoreLayers = serializedObject.FindProperty("IgnoreLayers");

        sensor = (ConeSensor)target;
        if (sensor != null)
        {
            sensorTransform = sensor.transform;
        }
    }

    private bool losFoldout;
    private bool eventFoldout;
    private bool gizmoFoldout;
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.PropertyField(radius);
        EditorGUILayout.PropertyField(allowedAngle);
        EditorGUILayout.PropertyField(maximalDetectableObjects);
        EditorGUILayout.EndVertical();

        losFoldout = EditorGUILayout.Foldout(losFoldout, "Line of sight");
        if (losFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(UseLineOfSight);
            EditorGUILayout.PropertyField(precision);
            EditorGUILayout.PropertyField(visibilityThreshold);
            EditorGUILayout.PropertyField(IgnoreLayers);
            EditorGUILayout.EndVertical();
        }

        eventFoldout = EditorGUILayout.Foldout(eventFoldout, "Events");
        if (eventFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(OnColliderFound);
            EditorGUILayout.PropertyField(OnColliderEnter);
            EditorGUILayout.PropertyField(OnColliderStay);
            EditorGUILayout.PropertyField(OnColliderExit);
            EditorGUILayout.EndVertical();
        }

        gizmoFoldout = EditorGUILayout.Foldout(gizmoFoldout, "Gizmos");
        if (gizmoFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(alwaysDrawGizmos);
            EditorGUILayout.PropertyField(arcColor);
            EditorGUILayout.PropertyField(gizmoColor);
            EditorGUILayout.EndVertical();
        }

        serializedObject.ApplyModifiedProperties();
    }

}
