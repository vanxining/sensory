﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(SphereSensor))]
public class SphereSensorEditor : Editor
{
    SerializedProperty precision;
    SerializedProperty visibilityThreshold;
    SerializedProperty UseLineOfSight;
    SerializedProperty IgnoreLayers;

    SerializedProperty radius;

    SerializedProperty OnColliderFound;
    SerializedProperty OnColliderEnter;
    SerializedProperty OnColliderStay;
    SerializedProperty OnColliderExit;

    SerializedProperty maximalDetectableObjects;
    SerializedProperty alwaysDrawGizmos;
    SerializedProperty gizmoColor;

    static SphereSensor sensor;
    static Transform sensorTransform;

    void OnEnable()
    {
        precision = serializedObject.FindProperty("precision");
        visibilityThreshold = serializedObject.FindProperty("visibilityThreshold");
        UseLineOfSight = serializedObject.FindProperty("UseLineOfSight");
        IgnoreLayers = serializedObject.FindProperty("IgnoreLayers");

        radius = serializedObject.FindProperty("radius");

        OnColliderFound = serializedObject.FindProperty("OnColliderFound");
        OnColliderEnter = serializedObject.FindProperty("OnColliderEnter");
        OnColliderStay = serializedObject.FindProperty("OnColliderStay");
        OnColliderExit = serializedObject.FindProperty("OnColliderExit");

        maximalDetectableObjects = serializedObject.FindProperty("maximalDetectableObjects");
        alwaysDrawGizmos = serializedObject.FindProperty("alwaysDrawGizmos");
        gizmoColor = serializedObject.FindProperty("gizmoColor");

        sensor = (SphereSensor)target;
        if (sensor != null)
        {
            sensorTransform = sensor.transform;
        }
    }

    private bool losFoldout;
    private bool eventFoldout;
    private bool gizmoFoldout;
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.PropertyField(radius);
        EditorGUILayout.PropertyField(maximalDetectableObjects);
        EditorGUILayout.EndVertical();

        losFoldout = EditorGUILayout.Foldout(losFoldout, "Line of sight");
        if (losFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(UseLineOfSight);
            EditorGUILayout.PropertyField(precision);
            EditorGUILayout.PropertyField(visibilityThreshold);
            EditorGUILayout.PropertyField(IgnoreLayers);
            EditorGUILayout.EndVertical();
        }
        eventFoldout = EditorGUILayout.Foldout(eventFoldout, "Events");
        if (eventFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(OnColliderFound);
            EditorGUILayout.PropertyField(OnColliderEnter);
            EditorGUILayout.PropertyField(OnColliderStay);
            EditorGUILayout.PropertyField(OnColliderExit);
            EditorGUILayout.EndVertical();
        }

        gizmoFoldout = EditorGUILayout.Foldout(gizmoFoldout, "Gizmos");
        if (gizmoFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(alwaysDrawGizmos);
            EditorGUILayout.PropertyField(gizmoColor);
            EditorGUILayout.EndVertical();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
