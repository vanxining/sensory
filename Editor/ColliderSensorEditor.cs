﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(ColliderSensor))]
public class ColliderSensorEditor : Editor
{
    SerializedProperty precision;
    SerializedProperty visibilityThreshold;
    SerializedProperty UseLineOfSight;
    SerializedProperty IgnoreLayers;

    SerializedProperty OnColliderEnter;
    SerializedProperty OnColliderStay;
    SerializedProperty OnColliderExit;

    SerializedProperty maximalDetectableObjects;

    static ColliderSensor sensor;
    static Transform sensorTransform;

    void OnEnable()
    {
        precision = serializedObject.FindProperty("precision");
        visibilityThreshold = serializedObject.FindProperty("visibilityThreshold");
        UseLineOfSight = serializedObject.FindProperty("UseLineOfSight");
        IgnoreLayers = serializedObject.FindProperty("IgnoreLayers");

        OnColliderEnter = serializedObject.FindProperty("OnColliderEnter");
        OnColliderStay = serializedObject.FindProperty("OnColliderStay");
        OnColliderExit = serializedObject.FindProperty("OnColliderExit");

        maximalDetectableObjects = serializedObject.FindProperty("maximalDetectableObjects");

        sensor = (ColliderSensor)target;
        if (sensor != null)
        {
            sensorTransform = sensor.transform;
        }
    }

    private bool losFoldout;
    private bool eventFoldout;
    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.PropertyField(maximalDetectableObjects);
        EditorGUILayout.EndVertical();

        losFoldout = EditorGUILayout.Foldout(losFoldout, "Line of sight");
        if (losFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(UseLineOfSight);
            EditorGUILayout.PropertyField(precision);
            EditorGUILayout.PropertyField(visibilityThreshold);
            EditorGUILayout.PropertyField(IgnoreLayers);
            EditorGUILayout.EndVertical();
        }
        eventFoldout = EditorGUILayout.Foldout(eventFoldout, "Events");
        if (eventFoldout)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(OnColliderEnter);
            EditorGUILayout.PropertyField(OnColliderStay);
            EditorGUILayout.PropertyField(OnColliderExit);
            EditorGUILayout.EndVertical();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
