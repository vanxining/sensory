# Sensory
This project implements sensors, which are scripts that allow the user to find
colliders in the scene in a specific area. Additionaly a line of sight 
functionality is included to allow simulating character vision.

# License
This project is licensed by the **MIT license**.

# Summary
This project contains the following sensors (in alphabetical order)
*  ***Advanced sensor*** A special sensor, allowing to abstract the data of any sensor and sort out colliders by user given conditions
*  ***Box sensor*** A sensor that scans for colliders in a box area
*  ***Collider sensor*** A sensor that uses attached collider components instead of a fixed area shape to check for colliders
*  ***Cone sensor*** A sensor that uses a cone to to scan for colliders
*  ***Line sensor*** A sensor that checks for colliders in a straight line
*  ***Sphere sensor*** A sensor that scans for colliders in a sphere shape