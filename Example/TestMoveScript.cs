﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMoveScript : MonoBehaviour
{
    public void LogFoundCollider(Collider col)
    {
        Debug.Log("Found: " + col.name);
    }

    public void LogEnterCollider(Collider col)
    {
        Debug.Log("Enter: " + col.name);
    }

    public void LogStayCollider(Collider col)
    {
        Debug.Log("Stay: " + col.name);
    }

    public void LogExitCollider(Collider col)
    {
        Debug.Log("Exit: " + col.name);
    }
}
