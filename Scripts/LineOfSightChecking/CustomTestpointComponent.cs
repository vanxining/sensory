﻿using UnityEngine;

/// <summary>
/// If a gameobject has this component attached any sensor sensing this object will use the 
/// stored transforms for line of sight checking. This helps creating a more robust check
/// than the random point selection that is normaly used
/// </summary>
[AddComponentMenu("Sensor/LOS testing/LOS Point Component")]
public class CustomTestpointComponent : MonoBehaviour
{
    [Tooltip("Add all testpoints you want to use to this list. These will be used for visibility testing of the gameobject using Sensors")]
    public Transform[] testpoints;

    /// <summary>
    /// Return all testpoints this object contains (default if none)
    /// </summary>
    /// <returns></returns>
    public Vector3[] GetTestpoints()
    {
        if (testpoints == null || testpoints.Length == 0)
            return default;

        Vector3[] arr = new Vector3[testpoints.Length];
        for(int i = 0; i < testpoints.Length; i++)
        {
            arr[i] = testpoints[i].position;
        }

        return arr;
    }
}
