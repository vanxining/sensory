﻿/*--- Source: https://gist.github.com/v21/5378391 ---*/

using UnityEngine;

namespace Sensory
{
    namespace MeshHelperTools
    {
        /// <summary>
        /// This class allows to find random points on a mesh
        /// This is used for line of sight checking
        /// Since it uses random: This is not a precise system!
        /// </summary>
        public class MeshHelper
        {
            /// <summary>
            /// The used mesh
            /// </summary>
            private Mesh mesh;

            #region Mesh data
            private float[] sizes;
            private float[] cumulativeSizes;
            private float total;
            #endregion

            /// <summary>
            /// Create a new mesh helper
            /// </summary>
            /// <param name="mesh">The mesh you want to use</param>
            public MeshHelper(Mesh mesh)
            {
                this.mesh = mesh;

                sizes = GetTriSizes(mesh.triangles, mesh.vertices);
                cumulativeSizes = new float[sizes.Length];
                total = 0;

                for (int i = 0; i < sizes.Length; i++)
                {
                    total += sizes[i];
                    cumulativeSizes[i] = total;
                }
            }

            /// <summary>
            /// This method changes the used mesh.
            /// You can call this method instead of creating another MeshHelper. 
            /// </summary>
            /// <param name="mesh">The new mesh you would like to use</param>
            public void UpdateMesh(Mesh mesh)
            {
                if (mesh == null)
                    Debug.LogError("Cannot call UpdateMesh with an empty mesh!");

                this.mesh = mesh;

                //if you're repeatedly doing this on a single mesh, you'll likely want to cache cumulativeSizes and total
                sizes = GetTriSizes(mesh.triangles, mesh.vertices);
                cumulativeSizes = new float[sizes.Length];
                total = 0;

                for (int i = 0; i < sizes.Length; i++)
                {
                    total += sizes[i];
                    cumulativeSizes[i] = total;
                }
            }

            /// <summary>
            /// Returns a random point at a meshs surface
            /// </summary>
            /// <returns></returns>
            public Vector3 GetRandomPointOnMesh()
            {
                if (this.mesh == null)
                    Debug.LogError("Cannot call GetRandomPointOnMesh with a null mesh!");

                float randomsample = Random.value * total;

                int triIndex = -1;

                for (int i = 0; i < sizes.Length; i++)
                {
                    if (randomsample <= cumulativeSizes[i])
                    {
                        triIndex = i;
                        break;
                    }
                }

                if (triIndex == -1) Debug.LogError("triIndex should never be -1");

                Vector3 a = mesh.vertices[mesh.triangles[triIndex * 3]];
                Vector3 b = mesh.vertices[mesh.triangles[triIndex * 3 + 1]];
                Vector3 c = mesh.vertices[mesh.triangles[triIndex * 3 + 2]];

                //generate random barycentric coordinates

                float r = Random.value;
                float s = Random.value;

                if (r + s >= 1)
                {
                    r = 1 - r;
                    s = 1 - s;
                }
                //and then turn them back to a Vector3
                Vector3 pointOnMesh = a + r * (b - a) + s * (c - a);
                return pointOnMesh;

            }

            /// <summary>
            /// Returns the tri sizes
            /// </summary>
            /// <param name="tris"></param>
            /// <param name="verts"></param>
            /// <returns></returns>
            private float[] GetTriSizes(int[] tris, Vector3[] verts)
            {
                int triCount = tris.Length / 3;
                float[] sizes = new float[triCount];
                for (int i = 0; i < triCount; i++)
                {
                    sizes[i] = .5f * Vector3.Cross(verts[tris[i * 3 + 1]] - verts[tris[i * 3]], verts[tris[i * 3 + 2]] - verts[tris[i * 3]]).magnitude;
                }
                return sizes;
            }
        }

    }
}