﻿using System.Collections.Generic;
using UnityEngine;
using Sensory.Sensor;

#if UNITY_EDITOR
using UnityEditor;
#endif

[AddComponentMenu("Sensor/Basic Sensors/Box Sensor")]
public class BoxSensor : BasicSensor
{
    [Tooltip("The shape of the sensor")]
    public Vector3 boxExtents = new Vector3(1,1,1);

    [Range(0, 128), Tooltip("The maximum amount of objects that can be detected by this sensor per simulation step. Use 0 for infinite")]
    public int maximalDetectableObjects = 64;

    [Tooltip("Forces the sensor to draw its gizmos")]
    public bool alwaysDrawGizmos;
    [Tooltip("The color of all gizmos")]
    public Color gizmoColor = Color.yellow;

    /*
    //FixedUpdate is called in fixed intervals
    private void FixedUpdate()
    {
        SensorStep();
    }
    */

    /// <summary>
    /// Returns all collisions of this boxsensor
    /// </summary>
    /// <returns></returns>
    protected override Collider[] GetCollisions()
    {
        ClearDetectedColliders();

        return Physics.OverlapBox(transform.position, boxExtents / 2, transform.rotation);
    }

    /// <summary>
    /// All found (but possibly invalid) colliders
    /// </summary>
    Collider[] col;
    /// <summary>
    /// Simulate one sensor step
    /// </summary>
    public override void SensorStep()
    {
        col = GetCollisions();

        if(col != null && col.Length > 0)
        {
            foreach (Collider c in col)
            {
                if (HasColliderValidators)
                {
                    if (ColliderIsValid(c))
                    {
                        RegisterValidCollider(c);
                    }
                }
                else
                {
                    RegisterValidCollider(c);
                }
            }
        }

        FinalizeSimulationStep();
    }

    /// <summary>
    /// Setup this Boxsensor
    /// </summary>
    public override void SetupSensor()
    {
        if (maximalDetectableObjects > 0)
            DetectedColliders = new List<Collider>(maximalDetectableObjects);
        else if (maximalDetectableObjects == 0)
            DetectedColliders = new List<Collider>();
    }

    #region Handles
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (alwaysDrawGizmos)
        {
            DrawGizmos();
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (!alwaysDrawGizmos)
        {
            DrawGizmos();
        }
    }

    private void DrawGizmos()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = gizmoColor;
        Gizmos.DrawWireCube(Vector3.zero , boxExtents);

        DrawLineToCollider(this);
    }

    /// <summary>
    /// Draws a line to a collider if it is in the line of sight
    /// </summary>
    /// <param name="col">The colliders to draw to</param>
    /// <param name="transform">A reference to the object transform</param>
    private void DrawLineToCollider(BoxSensor scr)
    {
        Collider[] col = GetCollisions();
        Transform transform = scr.transform;

        if (col != null && col.Length > 0)
        {
            Handles.color = Color.cyan;

            Vector3 target;
            foreach (Collider c in col)
            {
                target = c.transform.position;

                if (scr.HasColliderValidators && scr.ColliderIsValid(c))
                {
                    if (UseLineOfSight) //If we use line of sight, we will need to check it
                    {
                        //We can safely do this check because we know every other check was passed already
                        if (!LOS_IsVisibleUsingSensor(c, visibilityThreshold, false))
                            return; //Do not store invisible colliders
                    }

                    Handles.color = Color.cyan;
                    Handles.DrawLine(transform.position, target);
                }
                else if (!scr.HasColliderValidators)
                {
                    if (UseLineOfSight) //If we use line of sight, we will need to check it
                    {
                        //We can safely do this check because we know every other check was passed already
                        if (!LOS_IsVisibleUsingSensor(c, visibilityThreshold, false))
                            return; //Do not store invisible colliders
                    }

                    Handles.color = Color.cyan;
                    Handles.DrawLine(transform.position, target);
                    Handles.Label(target, c.name);
                }

            }
        }
    }
#endif
    #endregion

}
