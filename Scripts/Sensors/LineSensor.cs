﻿using UnityEngine;
using Sensory.Sensor;

#if UNITY_EDITOR
using UnityEditor;
#endif

[AddComponentMenu("Sensor/Basic Sensors/Line Sensor")]
public class LineSensor : BasicSensor
{
    [Range(0,25),Tooltip("The length of the casted line")]
    public float lineLength = 5f;

    [Tooltip("Set to true if you want to find all colliders in the line")]
    public bool CanPenetrate;

    [Tooltip("Forces the sensor to draw its gizmos")]
    public bool alwaysDrawGizmos;
    [Tooltip("The color of all gizmos")]
    public Color gizmoColor = Color.yellow;

    /*
    //FixedUpdate is called in fixed intervals
    private void FixedUpdate()
    {
        SensorStep();
    }
    */

    /// <summary>
    /// All found collisions
    /// </summary>
    Collider[] col;
    /// <summary>
    /// Perform one simulation step
    /// </summary>
    public override void SensorStep()
    {
        col = GetCollisions();

        if (col != null && col.Length > 0)
        {
            foreach (Collider c in col)
            {
                if (HasColliderValidators)
                {
                    if (ColliderIsValid(c))
                    {
                        RegisterValidCollider(c);
                    }
                }
                else
                {
                    RegisterValidCollider(c);
                }
            }
        }

        FinalizeSimulationStep();
    }

    /// <summary>
    /// Setup the linesensor
    /// </summary>
    public override void SetupSensor()
    {
        
    }

    /// <summary>
    /// The raycast hit information is stored in this variable
    /// </summary>
    private RaycastHit hit;
    /// <summary>
    /// Returns all possible collisions (since this is a raycast, it will only be one)
    /// </summary>
    /// <returns></returns>
    protected override Collider[] GetCollisions()
    {
        ClearDetectedColliders();

        if (!CanPenetrate)
        {
            if (Physics.Raycast(transform.position, transform.forward, out hit, lineLength, ~IgnoreLayers))
            {
                return new Collider[] { hit.collider };
            }
        }
        else //if(CanPenetrate)
        {
            RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.forward, lineLength, ~IgnoreLayers);

            if (hits.Length > 0)
            {
                Collider[] col = new Collider[hits.Length];

                for (int i = 0; i < col.Length; i++)
                {
                    col[i] = hits[i].collider;
                }

                return col;
            }
            else
            {
                return null;
            }
        }

        return null;
    }

    #region Handles
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (alwaysDrawGizmos)
        {
            DrawGizmos();
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (!alwaysDrawGizmos)
        {
            DrawGizmos();
        }
    }

    private void DrawGizmos()
    {
        Handles.color = gizmoColor;
        Handles.DrawLine(transform.position, transform.position + transform.forward.normalized * lineLength);
    }
#endif
    #endregion

}
