﻿using Sensory.Sensor;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The advanced sensor takes a BasicSensor and filters its output to make it easier to use
/// </summary>
[AddComponentMenu("Sensor/Advanced/Advanced Sensor")]
public class AdvancedSensor : MonoBehaviour
{
    #region Members
    [Tooltip("Attach the sensor you would like to use here. When you change the sensor, remember to call SetupAdvancedSensor!")]
    public BasicSensor sensor;

    [Tooltip("Time in seconds after which the sensor gets refreshed. 0 = every frame update (if not using fixed update, then every fixed time step)")]
    public float refreshRate = .1f;
    /// <summary>
    /// This variable contains a timestamp of when the refresh was last triggered
    /// </summary>
    private float tmp;

    [Tooltip("The allowed layers")]
    public LayerMask checkForLayers;

    [Tooltip("The tag you want to search")]
    public string tagSearch = "Untagged";
    #endregion

    #region UpdateMode
    public enum UpdateMode { FixedUpdate, Update, LateUpdate };
    public UpdateMode updateMode = UpdateMode.FixedUpdate;
    #endregion

    #region DetectionMode
    public enum DetectionMode { Collider, Rigidbody, Tag };
    [SerializeField]
    [Tooltip("Determines the detection mode of the sensor. Collider will look for anything with a collider. " +
             "Rididbody will only look for object with a rigidbody attached. Tag will look for Colliders with a given tag")]
    private DetectionMode SensorDetectionMode = DetectionMode.Collider;

    /// <summary>
    /// Returns the current SensorDetectionMode
    /// </summary>
    /// <returns></returns>
    public DetectionMode GetDetectionMode()
    {
        return SensorDetectionMode;
    }

    /// <summary>
    /// Do not directly set SensorDetectionMode since this can lead to weird behaviour. Use this method instead
    /// </summary>
    /// <param name="dm">The new detectionmode you would like to use</param>
    public void SetDetectionMode(DetectionMode dm)
    {
        switch (dm)
        {
            case DetectionMode.Collider:

                if(SensorDetectionMode == DetectionMode.Rigidbody) //Unset the rigidbody
                    UnsetRigidbodyMode();

                SensorDetectionMode = dm;
                break;
            case DetectionMode.Rigidbody:
                SetRigidbodyMode();
                SensorDetectionMode = dm;
                break;
            case DetectionMode.Tag:
                if (SensorDetectionMode == DetectionMode.Rigidbody) //Unset the rigidbody
                    UnsetRigidbodyMode();

                SensorDetectionMode = dm;
                break;
            default: break;
        }
    }
    #endregion

    #region Found objects
    /// <summary>
    /// All found colliders
    /// </summary>
    public Collider[] foundColliders;
    /// <summary>
    /// All found rigidbodies
    /// </summary>
    public Rigidbody[] foundRigidbodies;
    #endregion

    #region Setup / OnDestroy
    /// <summary>
    /// Call this method when you plan to change anything at run time. First apply your changes, then call this method
    /// </summary>
    public void SetupAdvancedSensor()
    {
        if (sensor == null) //Do not allow null
        {
            sensor = GetComponent<BasicSensor>(); //Try to find a sensor

            if (sensor == null) //If no sensor attached: Disable this script
            {
                Debug.LogWarning("No sensor attached at: " + name);
                this.enabled = false;
            }
        }

        sensor.SetupSensor(); //Setup the attached sensor

        if (SensorDetectionMode == DetectionMode.Rigidbody) //If the sensor should detect rigidbodies, set it up for that task
            SetRigidbodyMode();
        else
            UnsetRigidbodyMode(); //If we are not looking for rigidbodies make sure no mode is still activated

        if(SensorDetectionMode == DetectionMode.Tag)
            SetTagMode();
        else
            UnsetTagMode();

        tmp = Time.time; //Reset the timer

        SetLayerMode();
    }

    private void OnDestroy() //Remove all methods
    {
        if (SensorDetectionMode == DetectionMode.Rigidbody)
            UnsetRigidbodyMode();
        else if (SensorDetectionMode == DetectionMode.Tag)
            UnsetTagMode();

        UnsetLayerMode();
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        SetupAdvancedSensor();

        refreshRate = Mathf.Clamp(refreshRate, 0, float.MaxValue); //Clamp the refreshrate
            
    }

    #region Update methods
    // Update is called once per frame
    void FixedUpdate()
    {
        if(updateMode == UpdateMode.FixedUpdate)
        {
            AdvancedSensorStep();
        }
    }

    private void Update()
    {
        if (updateMode == UpdateMode.Update)
        {
            AdvancedSensorStep();
        }
    }

    private void LateUpdate()
    {
        if (updateMode == UpdateMode.LateUpdate)
        {
            AdvancedSensorStep();
        }
    }
    #endregion

    #region Main methods
    /// <summary>
    /// Performs one advanced (/more complex) simulation step
    /// </summary>
    private void AdvancedSensorStep()
    {
        if (Time.time >= tmp + refreshRate)
        {
            sensor.SensorStep();

            AdvancedSensorLogic();

            tmp = Time.time;
        }
    }

    /// <summary>
    /// Gets either all colliders or all rigidbodies in sensor range
    /// </summary>
    private void AdvancedSensorLogic()
    {
        switch (SensorDetectionMode)
        {
            case DetectionMode.Collider:
                FindColliders();
                break;
            case DetectionMode.Rigidbody:
                FindRigidbodies();
                break;
            case DetectionMode.Tag:
                FindColliders();
                break;
            default:
                break;
        }
    }
    #endregion

    #region Layer extensions
    /// <summary>
    /// Sets the layermode
    /// </summary>
    private void SetLayerMode()
    {
        if (!sensor.IsAlreadyRegistered(HasCorrectLayer))
        {
            sensor.ColliderValidator += HasCorrectLayer;
        }
        else
        {
            sensor.ColliderValidator -= HasCorrectLayer; //Remove the old layer-set
            sensor.ColliderValidator += HasCorrectLayer; //Insert the new layer-set
        }
    }
    /// <summary>
    /// Unsets the layermode
    /// </summary>
    private void UnsetLayerMode()
    {
        if (sensor.IsAlreadyRegistered(HasCorrectLayer))
            sensor.ColliderValidator -= HasCorrectLayer;
    }
    /// <summary>
    /// Returns true if the given collider has the same layer as the advanced sensor layers member
    /// </summary>
    /// <param name="col">The collider to check</param>
    /// <returns></returns>
    private bool HasCorrectLayer(Collider col)
    {
        return (((checkForLayers >> col.gameObject.layer) & 1) == 1); //Check if the found layer is selected
    }
    #endregion

    #region Rigidbody extension
    /// <summary>
    /// Sets the condition of any collider to have a rigidbody attached
    /// </summary>
    private void SetRigidbodyMode()
    {
        if (!sensor.IsAlreadyRegistered(HasRigidbody))
        {
            sensor.ColliderValidator += HasRigidbody;
        }
    }
    /// <summary>
    /// Removes the "must have rigidbody" condition
    /// </summary>
    private void UnsetRigidbodyMode()
    {
        if(sensor.IsAlreadyRegistered(HasRigidbody))
            sensor.ColliderValidator -= HasRigidbody;
    }
    /// <summary>
    /// Checks if a given collider has a rigidbody attached (if the object of the collider has both attached)
    /// </summary>
    /// <param name="c">The collider to check</param>
    /// <returns></returns>
    private bool HasRigidbody(Collider c)
    {
        return c.GetComponent<Rigidbody>()!=null;
    }
    #endregion

    #region Tag extensions
    /// <summary>
    /// Sets the condition of any collider to have a rigidbody attached
    /// </summary>
    private void SetTagMode()
    {
        if (!sensor.IsAlreadyRegistered(HasTag))
        {
            sensor.ColliderValidator += HasTag;
        }
        else
        {
            sensor.ColliderValidator -= HasTag; //Remove the old tag-set
            sensor.ColliderValidator += HasTag; //Insert the new tag-set
        }
    }
    /// <summary>
    /// Removes the "must have rigidbody" condition
    /// </summary>
    private void UnsetTagMode()
    {
        if (sensor.IsAlreadyRegistered(HasTag))
            sensor.ColliderValidator -= HasTag;
    }
    /// <summary>
    /// Checks if a given collider has a rigidbody attached (if the object of the collider has both attached)
    /// </summary>
    /// <param name="c">The collider to check</param>
    /// <returns></returns>
    private bool HasTag(Collider c)
    {
        return c.CompareTag(tagSearch);
    }
    #endregion

    #region Find all xxx
    /// <summary>
    /// Returns all found colliders (if any)
    /// </summary>
    private void FindColliders()
    {
        foundColliders = sensor.DetectedColliders.ToArray();
    }
    /// <summary>
    /// Returns all found Rigidbodies (if any)
    /// </summary>
    private void FindRigidbodies()
    {
        foundColliders = sensor.DetectedColliders.ToArray();

        List<Rigidbody> rb = new List<Rigidbody>();
        foreach(Collider col in foundColliders)
        {
            rb.Add(col.GetComponent<Rigidbody>());
        }

        foundRigidbodies = rb.ToArray();
    }
    #endregion

}
