﻿#define USE_MESH_HELPER

#if USE_MESH_HELPER
using Sensory.MeshHelperTools;
#endif

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Sensory
{

    namespace Sensor
    {
        /// <summary>
        /// This is the base of all sensor classes
        /// </summary>
        public abstract class BasicSensor : MonoBehaviour
        {
            /// <summary>
            /// This makes the setup automatic
            /// </summary>
            private void Awake()
            {
                SetupSensor();
            }

            #region Public properties
            /// <summary>
            /// Contains all colliders seen by this sensor
            /// </summary>
            public List<Collider> DetectedColliders { get; protected set; } = new List<Collider>();
            /// <summary>
            /// Contains all colliders formerly seen by this sensor
            /// </summary>
            private List<Collider> OldDetectedColliders { get; set; } = new List<Collider>();
            /// <summary>
            /// Returns the number of found colliders
            /// </summary>
            public int DetectedCollidersCount { get { return DetectedColliders != null ? DetectedColliders.Count : 0; } }
            #endregion

            #region Valid collider registration
            /// <summary>
            /// Adds the collider to the list of DetectedColliders and triggers the OnColliderFound event
            /// </summary>
            /// <param name="c">The collider you want to register</param>
            protected void RegisterValidCollider(Collider c)
            {
#if USE_MESH_HELPER
                if (UseLineOfSight) //If we use line of sight, we will need to check it
                {
                    //We can safely do this check because we know every other check was passed already
                    if (!LOS_IsVisibleUsingSensor(c, visibilityThreshold, false))
                        return; //Do not store invisible colliders
                }
#endif

                DetectedColliders.Add(c);
                OnColliderFound?.Invoke(c);

                //invoke some methods for entering or staying colliders
                if (OldDetectedColliders.Contains(c))
                {
                    OnColliderStay?.Invoke(c);
                }
                else //if(!OldDetectedColliders.Contains(c))
                {
                    OnColliderEnter?.Invoke(c);
                }

            }


            /// <summary>
            /// This method should get called at the beginning of GetCollision to ensure no clutter is left in the collider list
            /// </summary>
            protected void ClearDetectedColliders()
            {
                //Read the found colliders before deleting them
                OldDetectedColliders.Clear();
                OldDetectedColliders.AddRange(DetectedColliders);

                DetectedColliders.Clear();
            }
            #endregion

            #region Additional Methods
            /// <summary>
            /// This method should get called at the end of a simulation step to trigger the OnColliderExit event
            /// </summary>
            protected void FinalizeSimulationStep()
            {
                foreach(Collider c in OldDetectedColliders)
                {
                    if (!DetectedColliders.Contains(c))
                        OnColliderExit?.Invoke(c);
                }
            }
            #endregion

            #region Abstract methods
            /// <summary>
            /// This method should setup all values that need to be setup
            /// </summary>
            public abstract void SetupSensor();

            /// <summary>
            /// Should refresh the values (colliders etc.) of the sensor
            /// </summary>
            public abstract void SensorStep();

            /// <summary>
            /// Should return all collision that are found by the sensor (no matter if they are valid or not)
            /// </summary>
            /// <returns></returns>
            protected abstract Collider[] GetCollisions();
            #endregion

            #region Events
            [Tooltip("This event is triggered when a collider is found that matches all validators (this is called for ALL found colliders!")]
            public ColliderEvent OnColliderFound = new ColliderEvent();

            [Tooltip("This event is triggered when a collider is found that was not valid or visible the last update (called on first enter)")]
            public ColliderEvent OnColliderEnter = new ColliderEvent();

            [Tooltip("This event is triggered when a collider is found that already was valid the last update (earliest call: next update)")]
            public ColliderEvent OnColliderStay = new ColliderEvent();

            [Tooltip("This event is triggered when a leaves the sensor (called on leave)")]
            public ColliderEvent OnColliderExit = new ColliderEvent();
            #endregion

            #region Distance checker methods

            #region Closest collider search
            /// <summary>
            /// Returns the nearest collider to the given gameobject (the sensors gameobject by default)
            /// </summary>
            /// <param name="go">The gameobject you would like to use as a relative location</param>
            /// <returns></returns>
            public Collider GetNearest(GameObject go = null)
            {
                if (go == null)
                    go = this.gameObject;

                return GetNearest(go.transform.position);
            }

            /// <summary>
            /// Returns the nearest object to the sensor (which is still detected by it)
            /// </summary>
            /// <param name="transform">You can set a transform as the reference for distance checking</param>
            /// <returns></returns>
            public Collider GetNearest(Transform transform)
            {
                return GetNearest(transform.position);
            }

            /// <summary>
            /// Returns the nearest collider to a given position (the first one found on a tie)
            /// </summary>
            /// <param name="vector">The position you want to use for distance checking</param>
            /// <returns></returns>
            public Collider GetNearest(Vector3 vector)
            {
                if (DetectedCollidersCount == 0)
                    return null;
                else if (DetectedCollidersCount == 1)
                    return DetectedColliders[0];

                float tmp; //Store the new distance here
                float nearest = Vector3.Distance(DetectedColliders[0].transform.position, vector); //First approach: The 0th element is closest
                Collider res = DetectedColliders[0]; //Store the first collider
                for (int i = 1; i < DetectedCollidersCount; i++)
                {
                    tmp = Vector3.Distance(DetectedColliders[i].transform.position, vector); //Get the new distance
                    if (tmp < nearest) //If the new distance is smaller, store the collider as new closest
                    {
                        nearest = tmp;
                        res = DetectedColliders[i];
                    }
                }

                return res; //Return the closest collider
            }

            /// <summary>
            /// Returns the collider that is nearest to the sensor
            /// </summary>
            /// <returns></returns>
            public Collider GetNearestToSensor()
            {
                return GetNearest();
            }
            #endregion

            #region Furthest collider search
            /// <summary>
            /// Returns the collider that is furthest away (the first one found on a tie)
            /// </summary>
            /// <param name="vector">The position you want to use as reference for distance checking</param>
            /// <returns></returns>
            public Collider GetFurthest(Vector3 vector)
            {
                if (DetectedCollidersCount == 0)
                    return null;
                else if (DetectedCollidersCount == 1)
                    return DetectedColliders[0];

                float tmp; //Store the new distance here
                float nearest = Vector3.Distance(DetectedColliders[0].transform.position, vector); //First approach: The 0th element is furthest
                Collider res = DetectedColliders[0]; //Store the first collider
                for (int i = 1; i < DetectedCollidersCount; i++)
                {
                    tmp = Vector3.Distance(DetectedColliders[i].transform.position, vector); //Get the new distance
                    if (tmp > nearest) //If the new distance is larger, store the collider as new furthest
                    {
                        nearest = tmp;
                        res = DetectedColliders[i];
                    }
                }

                return res; //Return the furthest collider
            }

            /// <summary>
            /// Returns the furthest object to the sensor (which is still detected by it)
            /// </summary>
            /// <param name="transform">You can set a transform as the reference for distance checking</param>
            /// <returns></returns>
            public Collider GetFurthest(Transform transform)
            {
                return GetFurthest(transform.position);
            }

            /// <summary>
            /// Returns the furthest collider to a given position (the first one found on a tie)
            /// </summary>
            /// <param name="go">The gameobject you would like to use as a relative location</param>
            /// <returns></returns>
            public Collider GetFurthest(GameObject go = null)
            {
                if (go == null)
                    go = this.gameObject;

                return GetFurthest(go.transform.position);
            }

            /// <summary>
            /// Returns the furthest found collider relative to the sensor
            /// </summary>
            /// <returns></returns>
            public Collider GetFurthestToSensor()
            {
                return GetFurthest(gameObject.transform.position);
            }
            #endregion

            #endregion

            #region Validator delegates
            /// <summary>
            /// This delegate is made to sort out invalid colliders
            /// </summary>
            /// <param name="col">The found collider</param>
            /// <returns></returns>
            public delegate bool IsValid(Collider col);
            /// <summary>
            /// You can add methods to this event to sort out invalid 
            /// colliders from the found ones. If you want to
            /// get all colliders, simply leave this event empty
            /// <para>Remember that you can also add lambda expressions to an event like this:</para>
            /// <code>this.ColliderValidator += (Collider c) => ... ;</code>
            /// </summary>
            public event IsValid ColliderValidator;
            /// <summary>
            /// Returns true if any collider validators are set
            /// </summary>
            public bool HasColliderValidators { get { return ColliderValidator != null; } }

            /// <summary>
            /// Returns true if a collider is validated by ALL ColliderValidators, else false
            /// <para>If no validators exist, false is also returned</para>
            /// </summary>
            /// <param name="c">The collider to test</param>
            /// <returns></returns>
            public bool ColliderIsValid(Collider c)
            {
                if (!HasColliderValidators)
                    return false;

                //TODO: Montior this for speed, this looks slow!
                bool tmp = true;
                foreach (Delegate item in ColliderValidator.GetInvocationList())
                {
                    tmp = tmp && (bool)item.DynamicInvoke(c);
                }

                return tmp;
            }

            /// <summary>
            /// Returns true if the collider gets validated by ANY validator
            /// </summary>
            /// <param name="c">The collider you would like to check</param>
            /// <returns></returns>
            public bool ColliderIsWeakValid(Collider c)
            {
                if (!HasColliderValidators)
                    return false;

                //TODO: Montior this for speed, this looks slow!
                bool tmp = false;
                foreach (Delegate item in ColliderValidator.GetInvocationList())
                {
                    tmp = tmp || (bool)item.DynamicInvoke(c);
                }

                return tmp;
            }

            /// <summary>
            /// Returns true if the given method is already registered in the ColliderValidator event
            /// </summary>
            /// <param name="delegateToTest">The delegate you would like to check</param>
            /// <returns></returns>
            public bool IsAlreadyRegistered(IsValid delegateToTest)
            {
                if (ColliderValidator != null)
                {
                    foreach (IsValid existingHandler in ColliderValidator.GetInvocationList())
                    {
                        if (existingHandler == delegateToTest)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            #region Additional Classes
            /// <summary>
            /// This class implements a unity event that is getting called once a valid collider is found
            /// </summary>
            [System.Serializable]
            public class ColliderEvent : UnityEvent<Collider> { }
            #endregion

            #endregion

            #region Line of sight checking
#if USE_MESH_HELPER
            /*
             * Note: The Meshhelper does not work on an exact basis! This can lead to differing results!
             */

            [Tooltip("If true, the sensor will use line of sight testing for each collider it finds to determine if it is seen")]
            public bool UseLineOfSight;

            [Range(0, 1000), Tooltip("This number defines the precision of the line of sight testing (it is the amount of rays used). Higher is better, but slower")]
            public int precision = 5;

            [Range(0,1),Tooltip("An object must have at least this value to be seen by a sensor")]
            public float visibilityThreshold = .5f;

            [Tooltip("When testing for visibility, these layers will be ignored")]
            public LayerMask IgnoreLayers;

            /// <summary>
            /// This array contains all testpoints for line of sight testing
            /// </summary>
            private Vector3[] testpoints;
            /// <summary>
            /// The meshhelper used for random testing
            /// </summary>
            MeshHelper meshHelper;

            /// <summary>
            /// Returns true if the visibility of the collider is greater or equal to the given threshold
            /// </summary>
            /// <param name="col">The collider you want to check</param>
            /// <param name="threshold">The threshold, this determines when the collider is visible. This is a value between 0 and 1</param>
            /// <param name="onlyUseDetectedColliders">If set to true, the collider will get checked if it is detected by the sensor. If not -1 is returned, else the method functions normal</param>
            /// <returns></returns>
            public bool LOS_IsVisibleUsingSensor(Collider col, float threshold = .5f, bool onlyUseDetectedColliders = true)
            {
                threshold = Mathf.Clamp(threshold, 0, 1); //Clamp the float value

                return threshold <= LOS_CheckVisibility(col, onlyUseDetectedColliders);
            }

            /// <summary>
            /// Returns a value between 0 and 1 defining the visibility of the object
            /// </summary>
            /// <param name="col">The collider you want to check</param>
            /// <param name="onlyUseDetectedColliders">If set to true, the collider will get checked if it is detected by the sensor. If not -1 is returned, else the method functions normal</param>
            /// <returns></returns>
            public float LOS_CheckVisibility(Collider col, bool onlyUseDetectedColliders = true)
            {
                if (onlyUseDetectedColliders)
                {
                    if (!DetectedColliders.Contains(col))
                        return -1f;
                }

                if (col.TryGetComponent(out MeshFilter filter)) //Try to get a meshfilter (which contains the mesh)
                {
                    if (filter.sharedMesh == null) //Check for validity
                    {
                        Debug.LogError("Meshfilter found, but no mesh was attached at collider: " + col);
                        return 0;
                    }

                    if (meshHelper == null)
                        meshHelper = new MeshHelper(filter.sharedMesh); //Create a new Meshhelper
                    else
                        meshHelper.UpdateMesh(filter.sharedMesh); //Use the old mesh filter

                    CustomTestpointComponent ctc; //Try to get the testpoint component
                    if ((ctc = filter.GetComponent<CustomTestpointComponent>()) == null)
                    {

                        testpoints = new Vector3[precision]; //Create new testpoints

                        for (int i = 0; i < precision; i++)
                        {
                            testpoints[i] = col.transform.position + 
                                Vector3.Scale(meshHelper.GetRandomPointOnMesh(), col.transform.localScale); //Ensure the scale works correctly
                        }
                    }
                    else
                    {
                        testpoints = ctc.GetTestpoints();

                        if (testpoints == default) //If nothing was returned, log an error
                            Debug.LogError("Error, no testpoints found at: " + filter.name);
                    }

                    int seen = 0; //The number of rays that saw the object unblocked
                    Vector3 dir = Vector3.zero; //The direction to the collider
                    foreach (Vector3 vec in testpoints) //We need to test each testpoint
                    {
                        dir = vec - transform.position; //Get the direction vector

                        //Ray ray = new Ray(transform.position, dir);
                        //Debug.DrawRay(ray.origin,ray.direction, Color.red);

                        //Check if we find a collider in this direction
                        if (Physics.Raycast(transform.position, dir, out RaycastHit hit, float.PositiveInfinity, ~IgnoreLayers))
                        {
                            if (hit.collider == col) //We found the same collider
                                seen++; //Count one up
                        }
                    }

                    return (float)seen / testpoints.Length; //Return the successful rays divided by the number of rays

                }

                return 0f; //If the target does not have a mesh => We are not able to see it, no visibility!
            }

            /// <summary>
            /// Returns the visibility of all colliders that were found (in order of DetectedCollider entries)
            /// </summary>
            /// <returns></returns>
            public float[] LOS_GetVisibilities()
            {
                float[] res = new float[DetectedCollidersCount];
                for (int i = 0; i < DetectedCollidersCount; i++)
                {
                    res[i] = LOS_CheckVisibility(DetectedColliders[i]);
                }

                return res;
            }

#endif
            #endregion

        }
    }
}