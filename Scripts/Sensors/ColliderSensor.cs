﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sensory.Sensor;

public class ColliderSensor : BasicSensor
{
    [Range(0, 128), Tooltip("The maximum amount of objects that can be detected by this sensor per simulation step. Use 0 for infinite")]
    public int maximalDetectableObjects = 64;

    private void Start()
    {
        Collider[] col = GetComponents<Collider>();
        if (col != null && col.Length > 0)
        {
            foreach (Collider c in col)
            {
                c.isTrigger = true; //Set all colliders to triggers
            }
        }
    }

    public override void SensorStep()
    {
        
    }

    public override void SetupSensor()
    {
        if (maximalDetectableObjects > 0)
            DetectedColliders = new List<Collider>(maximalDetectableObjects);
        else if (maximalDetectableObjects == 0)
            DetectedColliders = new List<Collider>();
    }

    protected override Collider[] GetCollisions()
    {
        return null;
    }

    private void OnTriggerEnter(Collider c)
    {
        if (HasColliderValidators)
        {
            if (ColliderIsValid(c))
            {
                RegisterValidCollider(c);
            }
        }
        else
        {
            RegisterValidCollider(c);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (DetectedColliders.Contains(other)) //Was seen last update
        {
            if (HasColliderValidators)
            {
                if (ColliderIsValid(other))
                {
                    if (UseLineOfSight) //If we use line of sight, we will need to check it
                    {
                        //We can safely do this check because we know every other check was passed already
                        if (!LOS_IsVisibleUsingSensor(other, visibilityThreshold, false))
                        {
                            DetectedColliders.Remove(other);
                            OnColliderExit.Invoke(other);
                            return; //Do not store invisible colliders
                        }
                    }

                    OnColliderStay?.Invoke(other);
                }
                else
                {
                    DetectedColliders.Remove(other);
                    OnColliderExit.Invoke(other);
                }
            }
            else
            {
                //RegisterValidCollider(other);
            }
        }
        else
        {
            if (HasColliderValidators)
            {
                if (ColliderIsValid(other))
                {
                    RegisterValidCollider(other);
                }
            }
            else
            {
                RegisterValidCollider(other);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        DetectedColliders.Remove(other);
        OnColliderExit?.Invoke(other);
    }

    public void LogEnter(Collider c)
    {
        Debug.Log(c + " has entered");
    }

    public void LogStay(Collider c)
    {
        Debug.Log(c + " stayed");
    }

    public void LogExit(Collider c)
    {
        Debug.Log(c + " has exited");
    }
}
