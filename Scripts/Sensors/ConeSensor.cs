﻿using System.Collections.Generic;
using UnityEngine;
using Sensory.Sensor;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// This sensor uses up to half a sphere to find objects (or more over, there colliders) in sight
/// </summary>
[AddComponentMenu("Sensor/Basic Sensors/Cone Sensor")]
public class ConeSensor : BasicSensor
{
    #region Members
    [Range(0,50), Tooltip("The radius of the detection sphere")]
    public float radius = 1f;

    [Range(0, 179), Tooltip("The sensor vision angle")]
    public float angle = 45f;

    [Range(0,128), Tooltip("The maximum amount of objects that can be detected by this sensor per simulation step. Use 0 for infinite")]
    public int maximalDetectableObjects = 64;

    [Tooltip("Forces the sensor to draw its gizmos")]
    public bool alwaysDrawGizmos;
    [Tooltip("The color of the arc")]
    public Color arcColor = new Color(.2f, .2f, .2f, .2f);
    [Tooltip("The color of all gizmos")]
    public Color gizmoColor = Color.yellow;
    #endregion

    /*
    //FixedUpdate is called in fixed intervals
    private void FixedUpdate()
    {
        SensorStep();
    }
    */

    #region Inheritance Main Methods
    /// <summary>
    /// Simulate one step for this sensor
    /// </summary>
    public override void SensorStep()
    {
        CheckForIntersect();
        FinalizeSimulationStep();
    }

    /// <summary>
    /// Sets the sensor up. Call this in Start or Awake
    /// </summary>
    public override void SetupSensor()
    {
        if(maximalDetectableObjects > 0)
            DetectedColliders = new List<Collider>(maximalDetectableObjects);
        else if(maximalDetectableObjects == 0)
            DetectedColliders = new List<Collider>();
    }
    #endregion

    #region Main Sensor Method
    /// <summary>
    /// Contains all potentialy valid colliders, found by the sensor
    /// </summary>
    protected Collider[] col;
    /// <summary>
    /// Checks for objects in vision (removes all old objects)
    /// </summary>
    private void CheckForIntersect()
    {
        col = GetCollisions();

        if (col != null && col.Length > 0)
        {
            //We found something within the sphere
            foreach (Collider c in col) //Go through all colliders and check if they are within bounds
            {
                if (IsVisibleToSensor(c, transform))
                {
                    if (DetectedColliders.Count < maximalDetectableObjects)//Check if we have space for more objects
                    {
                        if (HasColliderValidators && ColliderIsValid(c))
                        {
                            RegisterValidCollider(c);
                        }
                        else if(!HasColliderValidators)
                        {
                            RegisterValidCollider(c);
                        }
                    }
                }
            }
           
        }
    }
    #endregion

    #region Sensor Functionality

    #region Sensor members
    /// <summary>
    /// The position of the collider
    /// </summary>
    private Vector3 target = new Vector3();
    #endregion

    /// <summary>
    /// Returns true if the given collider is visible to the sensor
    /// </summary>
    /// <param name="collider">The collider you want to check</param>
    /// <returns></returns>
    public bool IsVisibleToSensor(Collider collider, Transform reference = null)
    {
        target = collider.transform.position; //Get the collider position

        reference = reference ?? this.transform;

        if (!IsInFrontOf(target, transform.position, reference)) //Only allow objects in front of the sensor
        {
            return false;
        }

        float angle = Vector3.Angle(transform.forward, target - transform.position);

        if (angle <= this.angle/2)
            return true;

        return false;
    }

    #endregion

    #region Helper methods
    /// <summary>
    /// Get the colliders within bounds
    /// </summary>
    protected override Collider[] GetCollisions()
    {
        ClearDetectedColliders(); //Clear last collisions

        return Physics.OverlapSphere(transform.position, radius); //Get new potential collisions
    }

    /// <summary>
    /// Returns true if vec1 is in front of vec2
    /// </summary>
    /// <param name="vec1"></param>
    /// <param name="vec2"></param>
    /// <returns></returns>
    private bool IsInFrontOf(Vector3 vec1, Vector3 vec2, Transform reference)
    {
        return Vector3.Dot(vec1-vec2, reference.forward) >= 0;
    }
    #endregion

    #region Handles
#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        if(alwaysDrawGizmos)
            DrawHandles(this);
    }

    private void OnDrawGizmosSelected()
    {
        if(!alwaysDrawGizmos)
            DrawHandles(this);
    }

    readonly float boxSize = .05f;
    private Collider[] coll;
    /// <summary>
    /// Draw all the gizmos
    /// </summary>
    /// <param name="transform">A reference to the object transform</param>
    private void DrawHandles(ConeSensor scr)
    {
        Transform transform = scr.transform;
        Handles.color = Gizmos.color = scr.gizmoColor;
        Handles.DrawLine(transform.position, transform.position + transform.forward * scr.radius);

        Handles.CubeHandleCap(0, transform.position + transform.forward * scr.radius, Quaternion.identity, boxSize, EventType.Repaint);

        coll = Physics.OverlapSphere(transform.position, scr.radius);

        DrawVisuals(scr);

        DrawLineToCollider(scr);
    }

    /// <summary>
    /// Draw the visible lines and the arc
    /// </summary>
    /// <param name="transform">A reference to the object transform</param>
    private void DrawVisuals(ConeSensor scr)
    {
        Transform transform = scr.transform;

        Vector3[] lineEndings = new Vector3[]
        {
            (transform.rotation * Quaternion.Euler(0,scr.angle / 2 - 90, 0) * Vector3.right).normalized * scr.radius,
            (transform.rotation * Quaternion.Euler(0, -(scr.angle / 2 + 90), 0) * Vector3.right).normalized * scr.radius,
            (transform.rotation * Quaternion.Euler(scr.angle / 2, 0, 0) * Vector3.forward).normalized * scr.radius,
            (transform.rotation * Quaternion.Euler(-(scr.angle / 2), 0, 0) * Vector3.forward).normalized * scr.radius
        };


        Handles.color = scr.arcColor;

        float arcRadius = scr.radius * Mathf.Tan((scr.angle / 2) * Mathf.Deg2Rad);
        Handles.DrawSolidArc(transform.position + transform.forward * scr.radius, transform.forward, transform.right, 360, arcRadius);

        Handles.color = Gizmos.color = scr.gizmoColor;
        Handles.DrawWireArc(transform.position + transform.forward * scr.radius, transform.forward, transform.right, 360, arcRadius);

        float lineLength = scr.radius / (Mathf.Cos((scr.angle / 2) * Mathf.Deg2Rad));
        Handles.DrawLine(transform.position, transform.position + (lineEndings[0]).normalized * lineLength);
        Handles.CubeHandleCap(0, transform.position + (lineEndings[0]).normalized * lineLength, Quaternion.identity, boxSize, EventType.Repaint);
        Handles.DrawLine(transform.position, transform.position + (lineEndings[1]).normalized * lineLength);
        Handles.CubeHandleCap(0, transform.position + (lineEndings[1]).normalized * lineLength, Quaternion.identity, boxSize, EventType.Repaint);

        Handles.DrawLine(transform.position, transform.position + (lineEndings[2]).normalized * lineLength);
        Handles.CubeHandleCap(0, transform.position + (lineEndings[2]).normalized * lineLength, Quaternion.identity, boxSize, EventType.Repaint);
        Handles.DrawLine(transform.position, transform.position + (lineEndings[3]).normalized * lineLength);
        Handles.CubeHandleCap(0, transform.position + (lineEndings[3]).normalized * lineLength, Quaternion.identity, boxSize, EventType.Repaint);

    }

    /// <summary>
    /// Draws a line to a collider if it is in the line of sight
    /// </summary>
    /// <param name="col">The colliders to draw to</param>
    /// <param name="transform">A reference to the object transform</param>
    private void DrawLineToCollider(ConeSensor scr)
    {
        Collider[] col = coll;
        Transform transform = scr.transform;

        if (col != null && col.Length > 0)
        {
            Handles.color = Color.cyan;

            Vector3 target;
            foreach (Collider c in col)
            {
                target = c.transform.position;

                if (scr.IsVisibleToSensor(c, transform))
                {
                    if (scr.DetectedColliders.Count < scr.maximalDetectableObjects)//Check if we have space for more objects
                    {
                        if (scr.HasColliderValidators && scr.ColliderIsValid(c))
                        {
                            Handles.color = Color.cyan;
                            Handles.DrawLine(transform.position, target);
                        }
                        else if (!scr.HasColliderValidators)
                        {
                            Handles.color = Color.cyan;
                            Handles.DrawLine(transform.position, target);
                            Handles.Label(target, c.name);
                        }
                    }
                }
            }
        }
    }
#endif
    #endregion

}
